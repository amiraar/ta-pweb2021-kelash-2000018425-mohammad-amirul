<?php
// membuat array asosiatif
$artikel = [
    "nama" => "Mohammad Amirul Kurniawan Putranto",
    "kelas" => "H",
    "nim" => 2000018425
];

// mencetak isi array assosiatif
echo "<h2>".$artikel["nama"]."</h2>";
echo "<p>NIM: ".$artikel["nim"]."</p>";
echo "<p>Kelas: ".$artikel["kelas"]."</p>";
?>