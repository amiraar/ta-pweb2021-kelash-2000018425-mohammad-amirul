<?php
// membuat array 2 dimensi yang berisi array asosiatif
$artikel = [
    [
        "judul" => "Menampilkan seluruh isi array dengan FOR dan FOREACH",
        "penulis" => "Mohammad Amirul Kurniawan Putranto"
    ],
    [
        "judul" => "Mencetak struktur array",
        "penulis" => "Mohammad Amirul Kurniawan Putranto"
    ],
    [
        "judul" => "PWeb2021-Tugas12: Server Side Programming: Array & Fungsi di PHP",
        "penulis" => "Mohammad Amirul Kurniawan Putranto"
    ]
];

// menampilkan array
foreach($artikel as $post){
    echo "<h2>".$post["judul"]."</h2>";
    echo "<p>".$post["penulis"]."<p>";
    echo "<hr>";
}
?> 