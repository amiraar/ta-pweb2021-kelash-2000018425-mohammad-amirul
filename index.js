const container = document.getElementById("container");
const signIn = document.getElementById("sign-in");
const signUp = document.getElementById("sign-up");
const forget = document.getElementById("forget");
const username = document.getElementById("username");
const password = document.getElementById("password");

setTimeout(() => {
  container.classList.add("sign-in");
}, 200);

const toggle = () => {
  container.classList.toggle("sign-in");
  container.classList.toggle("sign-up");
  container.classList.toggle("forget");
};

signIn.addEventListener("click", toggle);
signUp.addEventListener("click", toggle);
forget.addEventListener("click", toggle);

signIn = window.confirm("Anda ingin meneruskan?");
document.write("Jawaban anda: "+ signIn);
